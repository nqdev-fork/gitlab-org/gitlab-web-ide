# GitLab Web IDE

[**Example App**](https://gitlab-org.gitlab.io/gitlab-web-ide/) | [**Developer Guide**](./docs/dev/README.md) | [**GitLab VS Code Extension**](https://gitlab.com/gitlab-org/gitlab-vscode-extension)

## What is this?

This project builds the [`@gitlab/web-ide` npm package](https://www.npmjs.com/package/@gitlab/web-ide), used in the [main GitLab project](https://gitlab.com/gitlab-org/gitlab) to bootstrap GitLab's context-aware Web IDE.

## How to use the example?

Visit [the Pages Deployment](https://gitlab-org.gitlab.io/gitlab-web-ide/) or run the example locally with `yarn start:example`. See detailed instructions [here](./docs/dev/development_environment_setup.md#Setup).

### For Client-only WebIDE

1. Fill out the startup configuration form, or accept the default values:

   | Field        | Value                   |
   | ------------ | ----------------------- |
   | Type         | `Client only (Default)` |
   | GitLab URL   | `https://gitlab.com`    |
   | Project Path | `gitlab-org/gitlab`     |
   | Ref          | `master`                |

2. Click **Start GitLab Web IDE**

### For Remote Development WebIDE

1. Fill out the startup configuration form

   | Field            | Value                |
   | ---------------- | -------------------- |
   | Type             | `Remote Development` |
   | Remote Authority | `localhost:9888`     |
   | Host Path        | `/home/user/project` |
   | Connection Token | `<token>`            |

- or add query parameters to URL: `?remoteHost=localhost:9888&hostPath=/home/user/project&tkn=password`

2. Click **Start GitLab Web IDE**

## How to contribute?

Check out the [developer docs](./docs/dev/README.md).
