import type { ErrorMessage, StartRemoteMessage } from './messages';
import type { TrackingEvent } from './instrumentation';

export interface BaseConfigLinks {
  feedbackIssue: string;
  userPreferences: string;
  signIn: string;
}

export interface FontConfig {
  /**
    Used in the VS Code settings.json (e.g. as 'editor.fontFamily').
    It will also be used in the @font-face/font-family CSS attribute.
  */
  fontFamily: string;
  /**
    Specifies the mime type in preload link (type='font/${format}') and
    in the @font-face/src/format CSS attribute.
    For available values, check https://www.iana.org/assignments/media-types/media-types.xhtml#font
  */
  format: string;
  /**
    Contains the full URL of the font resource.
    It's used in the preload link and in the @font-face/src/url CSS attribute.
   */
  srcUrl: string;
}
export interface BaseConfig {
  baseUrl: string;
  nonce?: string;
  handleError?: (params: ErrorMessage['params']) => void;
  handleStartRemote?: (params: StartRemoteMessage['params']) => void;
  handleClose?: () => void;
  handleTracking?: (event: TrackingEvent) => void;
  links: BaseConfigLinks;
  /** Contains font to be used in VS Code Text Editors */
  editorFont?: FontConfig;
}

export interface ForkInfo {
  // This will be truthy if the fork exists
  ide_path?: string;

  // This will be truthy if the fork doesn't exist and the user can fork
  fork_path?: string;
}

export interface ClientOnlyConfig extends BaseConfig {
  // gitlabUrl - The URL of the GitLab instance
  gitlabUrl: string;

  // projectPath - The path_with_namespace of the project to open
  projectPath: string;

  // gitlabToken - Token to use to authenticate requests
  codeSuggestionsEnabled?: boolean;

  // gitlabToken - Token to use to authenticate requests
  gitlabToken: string;

  // filePath - Default file path to open
  filePath?: string;

  // mrId - If opening from an MR, the ID of the MR
  mrId?: string;

  // mrTargetProject - If opening from an MR, the project path of the MR
  mrTargetProject?: string;

  // ref - If not coming from an MR, the branch ref to open
  ref?: string;

  // httpHeaders - Extra headers to pass with api requests (for example, csrf headers)
  httpHeaders?: Record<string, string>;

  // forkInfo - Fork information for the given projectPath. This follows the pre-existing
  //            interface used in the old Web IDE
  //            https://gitlab.com/gitlab-org/gitlab/-/blob/dd1e70d3676891025534dc4a1e89ca9383178fe7/app/assets/javascripts/ide/stores/getters.js#L24
  forkInfo?: ForkInfo;

  // username - The current username for the GitLab context. This is used for things like
  //            generating default branch names.
  //            https://gitlab.com/gitlab-org/gitlab-web-ide/-/issues/82
  username?: string;

  // telemetryEnabled - This property is a boolean that indicates if telemetry is enabled
  //                      and bases its value on the web browser's do not track signal.
  //                    The single source of truth to determine if telemetry is enabled in
  //                    in the Web IDE is GitLab's tracking module.
  //                    See https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/assets/javascripts/tracking/tracker.js#L26
  telemetryEnabled?: boolean;
}
export interface RemoteConfig extends BaseConfig {
  remoteAuthority: string;
  connectionToken: string;
  hostPath: string;
}

export type AnyConfig = ClientOnlyConfig | RemoteConfig;

export type ConfigType = 'client-only' | 'remote';
