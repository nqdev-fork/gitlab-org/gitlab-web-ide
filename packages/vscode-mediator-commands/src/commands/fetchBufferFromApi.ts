import { DefaultGitLabClient } from '@gitlab/gitlab-api-client';
import { fetchBufferFromApi, GetBufferRequest } from '@gitlab/web-ide-interop';
import { VSBufferWrapper } from '../types';

export const commandFactory =
  (client: DefaultGitLabClient, bufferWrapper: VSBufferWrapper): fetchBufferFromApi =>
  async (request: GetBufferRequest) => {
    const buffer = await client.fetchBufferFromApi(request);

    const byteArray = new Uint8Array(buffer);

    return bufferWrapper(byteArray);
  };
