import { gitlab, ProjectUserPermissions } from '@gitlab/gitlab-api-client';
import type { InteropConfig } from '@gitlab/web-ide-interop';
import type { ClientOnlyConfig, ForkInfo } from '@gitlab/web-ide-types';

// region: GitLab API Types we pass through to the extension -----------

export type GitLabCommitPayload = gitlab.CommitPayload;
export type GitLabRepositoryTreeItem = gitlab.RepositoryTreeItem;
export type GitLabBranch = gitlab.Branch;
export type GitLabProject = gitlab.Project;

// region: Mediator specific types -------------------------------------

interface MergeRequestContext {
  // id - Global ID (not iid) of the MergeRequest
  id: string;

  // isMergeRequestBranch - true if the branch has a corresponding MR URL
  isMergeRequestBranch: boolean;

  // mergeRequestUrl - the existing MR URL of the branch
  mergeRequestUrl: string;

  // baseSha - the base SHA of the MergeRequest (used for viewing MR changes)
  baseSha: string;
}

export interface IFullConfig extends ClientOnlyConfig, InteropConfig {
  repoRoot: string;
}

export interface StartCommandOptions {
  ref?: string;
}

export interface StartCommandResponse {
  // gitlUrl - GitLab instance URL
  gitlabUrl: string;

  // project - A list of files used to initialize the file system
  files: GitLabRepositoryTreeItem[];

  // project - The GitLab Branch for the current Web IDE context
  branch: GitLabBranch;

  // project - The GitLab Project for the current Web IDE context
  project: GitLabProject;

  // repoRoot - the root path of the FileSystem where the main repository lives
  repoRoot: string;

  // userPermissions - current user permissions for the project
  userPermissions: ProjectUserPermissions;

  // mergeRequest - contains optional MergeRequest properties of the current Web IDE context
  mergeRequest?: MergeRequestContext;

  // forkInfo - fork info about the project from the Config
  forkInfo?: ForkInfo;
}

export interface ICommand {
  id: string;
  handler: (...args: never[]) => unknown;
}

export interface VSCodeBuffer {
  readonly buffer: Uint8Array;
}

export type VSBufferWrapper = (arg0: Uint8Array) => VSCodeBuffer;
